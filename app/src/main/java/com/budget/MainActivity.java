package com.budget;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;

import com.budget.adapter.FragmentAdapter;
import com.budget.service.BudgetService;

public class MainActivity extends AppCompatActivity {

    private BudgetService service;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = new BudgetService(this);
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager(), this,
                service);
        ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        PagerTabStrip pagerTitleStrip = findViewById(R.id.pager_title_strip);
        pagerTitleStrip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void CreateClick(View view) {
        EditText typeEditText = findViewById(R.id.TypeEditText);
        EditText sumEditText = findViewById(R.id.SumEditText);
        String type = typeEditText.getText().toString();
        double sum = Double.parseDouble(sumEditText.getText().toString());
        service.create(type, sum);
        Toast toast = Toast.makeText(getApplicationContext(), "Created", Toast.LENGTH_LONG);
        toast.show();
    }
}