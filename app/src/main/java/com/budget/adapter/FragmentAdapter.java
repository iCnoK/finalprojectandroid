package com.budget.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.budget.MainActivity;
import com.budget.fragment.CreateFragment;
import com.budget.fragment.ListFragment;
import com.budget.service.BudgetService;

import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {
    private final MainActivity mainActivity;

    private final BudgetService service;

    private List<String> names;

    public FragmentAdapter(@NonNull FragmentManager fm, MainActivity mainActivity,
                           BudgetService service) {
        super(fm);
        this.mainActivity = mainActivity;
        this.service = service;
        names = new ArrayList<>();
        names.add("Objects list");
        names.add("Create");
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return names.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListFragment(mainActivity, service);
            case 1:
                return new CreateFragment();
            default:
                return null;
        }
    }
}

