package com.budget.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.budget.domain.BudgetObject;

import java.util.List;

@Dao
public interface BudgetObjectDao {
    @Query("select * from budget")
    List<BudgetObject> getAll();

    @Query("select * from budget where id = :id")
    BudgetObject getById(Long id);

    @Insert
    void insert(BudgetObject object);

    @Update
    void update(BudgetObject object);

    @Delete
    void delete(BudgetObject object);
}
