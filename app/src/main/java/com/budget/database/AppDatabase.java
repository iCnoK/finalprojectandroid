package com.budget.database;

import androidx.room.RoomDatabase;

import com.budget.dao.BudgetObjectDao;
import com.budget.domain.BudgetObject;

@androidx.room.Database(entities = {BudgetObject.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract BudgetObjectDao budgetObjectDao();
}
