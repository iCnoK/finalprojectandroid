package com.budget.database;

import android.app.Application;

import androidx.room.Room;

public class Database extends Application {
    private static Database instance;

    private AppDatabase database;

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("LOL");
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .allowMainThreadQueries()
                .build();
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
