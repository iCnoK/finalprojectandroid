package com.budget.domain;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.budget.converter.LocalDateTimeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity(tableName = "budget")
public class BudgetObject {
    @PrimaryKey(autoGenerate = true)
    private Long id;

    private String type;

    @TypeConverters({LocalDateTimeConverter.class})
    private LocalDateTime date;

    private double sum;

    public BudgetObject(Long id, String type, LocalDateTime date, double sum) {
        this.id = id;
        this.type = type;
        this.date = date;
        this.sum = sum;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    @Override
    public String toString() {
        return "id: " + id + " type: " + type + " sum: " + sum + " date: " +
                date.format(DateTimeFormatter.ISO_DATE);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
