package com.budget.fragment;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.budget.MainActivity;
import com.budget.R;
import com.budget.domain.BudgetObject;
import com.budget.service.BudgetService;

import java.util.List;

public class ListFragment extends Fragment {
    private final MainActivity mainActivity;

    private final BudgetService service;

    private List<BudgetObject> budgetObjects;

    public ListFragment(MainActivity mainActivity, BudgetService service) {
        this.mainActivity = mainActivity;
        this.service = service;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        ListView objectsList = view.findViewById(R.id.ObjectsList);
        registerForContextMenu(objectsList);
        service.setObjectsList((ListView) view.findViewById(R.id.ObjectsList));
        service.setSumValue((TextView) view.findViewById(R.id.SumValue));
        budgetObjects = service.update();
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        mainActivity.getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getItemId() == R.id.DeleteButton) {
            budgetObjects = service.update();
            BudgetObject object = budgetObjects.get((int) info.id);
            this.service.delete(object);
            budgetObjects = service.update();
            Toast toast = Toast.makeText(mainActivity.getApplicationContext(),
                    "Deleted", Toast.LENGTH_LONG);
            toast.show();
        }
        return super.onContextItemSelected(item);
    }
}
