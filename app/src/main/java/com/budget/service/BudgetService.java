package com.budget.service;

import android.os.Build;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.budget.MainActivity;
import com.budget.database.AppDatabase;
import com.budget.database.Database;
import com.budget.domain.BudgetObject;

import java.time.LocalDateTime;
import java.util.List;

public class BudgetService {
    private final MainActivity mainActivity;

    private final AppDatabase database;

    private ListView objectsList;

    private TextView sumValue;

    public BudgetService(MainActivity activity) {
        this.database = Database.getInstance().getDatabase();
        this.mainActivity = activity;
    }

    public double sumAllObject() {
        List<BudgetObject> objects = database.budgetObjectDao().getAll();
        double result = 0;
        for (BudgetObject object : objects) {
            result += object.getSum();
        }
        return result;
    }

    public List<BudgetObject> getAll() {
        return database.budgetObjectDao().getAll();
    }

    public BudgetObject getById(Long id) {
        return database.budgetObjectDao().getById(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void create(String type, double sum) {
        LocalDateTime date = LocalDateTime.now();
        BudgetObject object = new BudgetObject(null, type, date, sum);
        insert(object);
        update();
    }

    public List<BudgetObject> update() {
        List<BudgetObject> objects = getAll();
        double sum = sumAllObject();
        ArrayAdapter<BudgetObject> adapter = new ArrayAdapter<>(mainActivity,
                android.R.layout.simple_list_item_1, objects);
        this.objectsList.setAdapter(adapter);
        this.sumValue.setText(String.valueOf(sum));
        return objects;
    }

    public void insert(BudgetObject object) {
        database.budgetObjectDao().insert(object);
    }

    public void update(BudgetObject object) {
        database.budgetObjectDao().update(object);
    }

    public void delete(BudgetObject object) {
        database.budgetObjectDao().delete(object);
    }

    public void setObjectsList(ListView objectsList) {
        this.objectsList = objectsList;
    }

    public void setSumValue(TextView sumValue) {
        this.sumValue = sumValue;
    }
}
